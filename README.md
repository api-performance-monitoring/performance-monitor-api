# Performance Monitor API
___
## API Project for integrating with the Performance Monitor API

The implementation of this can be over REST, JMS or any other technology desired. The aim is to have a API which is able to take high loads with out blocking the calling service. All work done with requests is asynchronous after being recieved.
 
When using this service never expect that it is always available as that puts your own application at risk. 
When using this service ensure to make your calling code asynchronous so as to keep your normal application flow risk free in-case of any speed issues with the service.

For an implementation Example go to: [gitlab.com/api-performance-monitoring/performance-monitor-api-service](https://gitlab.com/api-performance-monitoring/performance-monitor-api-service)

### Interface 
Simple interface to submit service call information
```
interface IPerformanceMonitor{
    public Boolean submit(ApiCall apiCall);
    public Boolean submitException(ApiCallException apiCallException);
}
```
### Models
Core Object (required). 
```
class ApiCall {
    private String applicationID;
    private String requestID;
    private String requestUser;
    private long requestStartTime;
    private long requestEndTime;
    private ApiCallMetaData apiCallMetaData;
}
```
Meta Data Object (optional but recommended). 
```
class ApiCallMetaData {
    private String webRequestURL;
    private String requestSenderIPAddress;
    private String requestProcessorIPAddress;
    private int httpResponseCode;
    private long requestSize;
    private long responseSize;
}
```
Exception Object (optional in the case you want to track exceptions in service).
```
class ApiCallException {
    private String applicationID;
    private String requestID;
    private String exceptionMessage;
    private String className;
    private Integer errorLineNumber;
}
```