package org.zorbitt.api;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ReflectionToStringBuilder;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * 
 * <br><b>Date Created</b>: 09 Mar 2016
 * @author ryan.zakariudakis
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ApiCallMetaData", propOrder = {
    "webRequestURL",
    "requestSenderIPAddress",
    "requestProcessorIPAddress",
    "httpResponseCode",
    "requestSize",
    "responseSize"
})
@ApiModel
public class ApiCallMetaData implements Serializable {
	private static final long serialVersionUID = 885201505229976261L;
	@ApiModelProperty
	@XmlElement(required = true)
	private String webRequestURL;
	@ApiModelProperty
	@XmlElement(required = true)
	private String requestSenderIPAddress;
	@ApiModelProperty
	@XmlElement(required = true)
	private String requestProcessorIPAddress;
	@ApiModelProperty
	@XmlElement(required = true)
	private int httpResponseCode;
	@ApiModelProperty
	@XmlElement(required = true)
	private long requestSize;
	@ApiModelProperty
	@XmlElement(required = true)
	private long responseSize;
	/**
	 * @return the webRequestURL - String
	 */
	public String getWebRequestURL() {
		return webRequestURL;
	}
	/**
	 * @param webRequestURL ({field_type}) the webRequestURL to set
	 */
	public void setWebRequestURL(String webRequestURL) {
		this.webRequestURL = webRequestURL;
	}
	/**
	 * @return the requestSenderIPAddress - String
	 */
	public String getRequestSenderIPAddress() {
		return requestSenderIPAddress;
	}
	/**
	 * @param requestSenderIPAddress ({field_type}) the requestSenderIPAddress to set
	 */
	public void setRequestSenderIPAddress(String requestSenderIPAddress) {
		this.requestSenderIPAddress = requestSenderIPAddress;
	}
	/**
	 * @return the requestProcessorIPAddress - String
	 */
	public String getRequestProcessorIPAddress() {
		return requestProcessorIPAddress;
	}
	/**
	 * @param requestProcessorIPAddress ({field_type}) the requestProcessorIPAddress to set
	 */
	public void setRequestProcessorIPAddress(String requestProcessorIPAddress) {
		this.requestProcessorIPAddress = requestProcessorIPAddress;
	}
	/**
	 * @return the httpResponseCode - int
	 */
	public int getHttpResponseCode() {
		return httpResponseCode;
	}
	/**
	 * @param httpResponseCode ({field_type}) the httpResponseCode to set
	 */
	public void setHttpResponseCode(int httpResponseCode) {
		this.httpResponseCode = httpResponseCode;
	}
	/**
	 * @return the requestSize - long
	 */
	public long getRequestSize() {
		return requestSize;
	}
	/**
	 * @param requestSize ({field_type}) the requestSize to set
	 */
	public void setRequestSize(long requestSize) {
		this.requestSize = requestSize;
	}
	/**
	 * @return the responseSize - long
	 */
	public long getResponseSize() {
		return responseSize;
	}
	/**
	 * @param responseSize ({field_type}) the responseSize to set
	 */
	public void setResponseSize(long responseSize) {
		this.responseSize = responseSize;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		return HashCodeBuilder.reflectionHashCode(this);
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		return EqualsBuilder.reflectionEquals(this, obj);
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return ReflectionToStringBuilder.reflectionToString(this);
	}
}