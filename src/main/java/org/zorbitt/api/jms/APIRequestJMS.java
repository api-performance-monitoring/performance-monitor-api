package org.zorbitt.api.jms;

/**
 * <br><b>Date Created</b>: 09 Mar 2016
 * @author ryan.zakariudakis
 */
public class APIRequestJMS {

	public static final String APIREQUEST_QUEUE = "queue/APIRequestQueue";
	public static final String APIREQUEST_DLQQUEUE = "queue/APIRequestQueueDLQ";
	public static final String APIREQUESTEXCEPTION_QUEUE = "queue/APIRequestQueueException";
	public static final String APIREQUESTEXCEPTION_DLQQUEUE = "queue/APIRequestQueueExceptionDLQ";
	
}
