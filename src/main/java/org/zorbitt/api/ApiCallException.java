package org.zorbitt.api;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ReflectionToStringBuilder;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * 
 * <br><b>Date Created</b>: 09 Mar 2016
 * @author ryan.zakariudakis
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ApiCallException", propOrder = {
		"applicationID",
	    "requestID",
	    "exceptionMessage",
	    "className",
	    "errorLineNumber"
	})
@ApiModel
public class ApiCallException implements Serializable {
	private static final long serialVersionUID = 885201505229976261L;
	@ApiModelProperty(notes="Application Key to group performance stats")
	@XmlElement(required = true)
	private String applicationID;
	@ApiModelProperty(notes="Individual unique request ID")
	@XmlElement(required = true)
	private String requestID;
	@ApiModelProperty
	@XmlElement(required = true)
	private String exceptionMessage;
	@ApiModelProperty
	@XmlElement(required = true)
	private String className;
	@ApiModelProperty
	@XmlElement(required = true)
	private Integer errorLineNumber;
	/**
	 * @return the applicationID - String
	 */
	public String getApplicationID() {
		return applicationID;
	}
	/**
	 * @param applicationID ({field_type}) the applicationID to set
	 */
	public void setApplicationID(String applicationID) {
		this.applicationID = applicationID;
	}
	/**
	 * @return the requestID - String
	 */
	public String getRequestID() {
		return requestID;
	}
	/**
	 * @param requestID ({field_type}) the requestID to set
	 */
	public void setRequestID(String requestID) {
		this.requestID = requestID;
	}
	/**
	 * @return the exceptionMessage - String
	 */
	public String getExceptionMessage() {
		return exceptionMessage;
	}
	/**
	 * @param exceptionMessage ({field_type}) the exceptionMessage to set
	 */
	public void setExceptionMessage(String exceptionMessage) {
		this.exceptionMessage = exceptionMessage;
	}
	/**
	 * @return the className - String
	 */
	public String getClassName() {
		return className;
	}
	/**
	 * @param className ({field_type}) the className to set
	 */
	public void setClassName(String className) {
		this.className = className;
	}
	/**
	 * @return the errorLineNumber - Integer
	 */
	public Integer getErrorLineNumber() {
		return errorLineNumber;
	}
	/**
	 * @param errorLineNumber ({field_type}) the errorLineNumber to set
	 */
	public void setErrorLineNumber(Integer errorLineNumber) {
		this.errorLineNumber = errorLineNumber;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		return HashCodeBuilder.reflectionHashCode(this);
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		return EqualsBuilder.reflectionEquals(this, obj);
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return ReflectionToStringBuilder.reflectionToString(this);
	}
}