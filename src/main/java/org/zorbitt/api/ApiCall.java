package org.zorbitt.api;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ReflectionToStringBuilder;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * 
 * <br><b>Date Created</b>: 09 Mar 2016
 * @author ryan.zakariudakis
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ApiCall", propOrder = {
	    "applicationID",
	    "requestID",
	    "requestUser",
	    "requestStartTime",
	    "requestEndTime",
	    "apiCallMetaData"
	})
@ApiModel
public class ApiCall implements Serializable {
	private static final long serialVersionUID = 885201505229976261L;
	@ApiModelProperty(notes="Application Key to group performance stats")
	@XmlElement(required = true)
	private String applicationID;
	@ApiModelProperty(notes="Individual unique request ID")
	@XmlElement(required = true)
	private String requestID;
	@ApiModelProperty(notes="User who initiated request")
	@XmlElement(required = true)
	private String requestUser;
	@ApiModelProperty(notes="Request Received by API")
	@XmlElement(required = true)
	private long requestStartTime;
	@ApiModelProperty(notes="API finished processing request")
	@XmlElement(required = true)
	private long requestEndTime;
	@ApiModelProperty(notes="Rich Data related to Call")
	@XmlElement(required = false)
	private ApiCallMetaData apiCallMetaData;
	/**
	 * @return the applicationID - String
	 */
	public String getApplicationID() {
		return applicationID;
	}
	/**
	 * @param applicationID ({field_type}) the applicationID to set
	 */
	public void setApplicationID(String applicationID) {
		this.applicationID = applicationID;
	}
	/**
	 * @return the requestID - String
	 */
	public String getRequestID() {
		return requestID;
	}
	/**
	 * @param requestID ({field_type}) the requestID to set
	 */
	public void setRequestID(String requestID) {
		this.requestID = requestID;
	}
	/**
	 * @return the requestUser - String
	 */
	public String getRequestUser() {
		return requestUser;
	}
	/**
	 * @param requestUser ({field_type}) the requestUser to set
	 */
	public void setRequestUser(String requestUser) {
		this.requestUser = requestUser;
	}
	/**
	 * @return the requestStartTime - long
	 */
	public long getRequestStartTime() {
		return requestStartTime;
	}
	/**
	 * @param requestStartTime ({field_type}) the requestStartTime to set
	 */
	public void setRequestStartTime(long requestStartTime) {
		this.requestStartTime = requestStartTime;
	}
	/**
	 * @return the requestEndTime - long
	 */
	public long getRequestEndTime() {
		return requestEndTime;
	}
	/**
	 * @param requestEndTime ({field_type}) the requestEndTime to set
	 */
	public void setRequestEndTime(long requestEndTime) {
		this.requestEndTime = requestEndTime;
	}
	/**
	 * @return the apiCallMetaData - ApiCallMetaData
	 */
	public ApiCallMetaData getApiCallMetaData() {
		return apiCallMetaData;
	}
	/**
	 * @param apiCallMetaData ({field_type}) the apiCallMetaData to set
	 */
	public void setApiCallMetaData(ApiCallMetaData apiCallMetaData) {
		this.apiCallMetaData = apiCallMetaData;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		return HashCodeBuilder.reflectionHashCode(this);
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		return EqualsBuilder.reflectionEquals(this, obj);
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return ReflectionToStringBuilder.reflectionToString(this);
	}
}