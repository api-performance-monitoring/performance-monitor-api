package org.zorbitt.api.rest;

import io.swagger.annotations.Api;

import javax.ejb.Local;
import javax.ws.rs.Consumes;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.zorbitt.api.ApiCall;
import org.zorbitt.api.ApiCallException;

/**
 * <br><b>Date Created</b>: 09 Mar 2016
 * @author ryan.zakariudakis
 */
@Path("monitor")
@Consumes({MediaType.TEXT_PLAIN, MediaType.APPLICATION_JSON})
@Local
@Api(value = "monitor", produces = MediaType.APPLICATION_JSON, consumes = MediaType.APPLICATION_JSON)
public interface IPerformanceMonitor {
	
	@PUT
	@Path("request/add")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Boolean submit(ApiCall apiCall);
	
	@PUT
	@Path("request/add/exception")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Boolean submitException(ApiCallException apiCallException);
}
